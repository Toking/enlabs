# enlabs

# Run
```shell script
git clone https://gitlab.com/Toking/enlabs
cd enlabs
docker-compose up
```

# Run test client
Test client calls /balance endpoint with random data for 1 minute with 5 seconds interval.
```shell script
go run cmd/client/main.go
```

# Create valid transaction
```shell script
curl --location --request POST 'http://127.0.0.1:8081/balance' \
--header 'Content-Type: application/json' \
--header 'Source-Type: game' \
--data-raw '
            {
               "state": "lost",
               "amount": "10.5",
               "transactionId": "beb4ad51-702d-4372-a7af-07801f430df2"
            }
'
```
