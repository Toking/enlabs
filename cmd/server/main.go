package main

import (
	"context"
	"gitlab.com/Toking/enlabs/api"
	"gitlab.com/Toking/enlabs/cronjobs"
	"gitlab.com/Toking/enlabs/database"
)

func main() {
	database.InitDB()
	route := api.NewRouter()
	apiEngine := api.New(route)
	cronjobs.GetInstance().Run(context.Background())
	apiEngine.Run(8081)
}