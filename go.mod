module gitlab.com/Toking/enlabs

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/jinzhu/gorm v1.9.16
)
